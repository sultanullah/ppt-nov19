package expression;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class FPExpression extends Expression {

	/**
	 * Constructor for FPExpression. This is a private constructor. Objects of
	 * this type can only be created by clients through a call to the static
	 * method 'getFPExpression(String)' or 'getFPExpression(Expression)'. This
	 * is to preserve the representation invariant and the sub-typing
	 * relationship.
	 * 
	 * @param expression
	 */
	private FPExpression(String expression) {
		//expression is any string
		//fpexpression is a string where every open bracket has a close bracket
		//super sets the the string expression private variable
		super(expression);
	}

	/**
	 * 
	 * @param expression
	 * @return an FPExpression object with the same input expression if the
	 *         input expression is fully parenthesized.
	 * @throws IllegalArgumentException
	 *             if the input is not a valid fully parenthesized expression.
	 */
	public static FPExpression getFPExpression(String expression)
			throws IllegalArgumentException {
		// TODO: Implement this method
		//return the input if all open brackets have a close bracket
		//else throw exception
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(expression);
	
		//iterate through the stringbuilder and find an open parentheses
		//delete the portion of the string builder up to the open parentheses
		//look for close parenthesis

	
		if(Character.toString(sb.charAt(0)).equals(")")){
			throw new IllegalArgumentException();
			
		} else {
		for(int i = 0; i < sb.length(); i++){
		if(Character.toString(sb.charAt(i)).equals("(")){
			sb.delete(0, sb.indexOf("(") + 1);
			for(int k = 0; k < sb.length(); k++){
				if(Character.toString(sb.charAt(k)).equals(")")){
					return new FPExpression(expression);
				
				}
			}
		}
	}
}
		return new FPExpression(expression);
}
			
		

		
		// If the input String is a valid fully parenthesized expression then
		// the following statement is correct.
		// This uses the private constructor.
		//return new FPExpression(expression);
		
		
	

	/**
	 * 
	 * @param expression
	 * @return an FPExpression object with the input expression string if the
	 *         input expression is fully parenthesized. 
	 * @throws IllegalArgumentException
	 *             if the input is not a valid fully parenthesized expression.
	 */
	public static FPExpression getFPExpression(Expression expression)
			throws IllegalArgumentException {
		// TODO: Implement this method. Somewhat similar to the method above.

		return null; // change this for sure!

	}

	/**
	 * Given an Expression, check if the expression is a fully parenthesized
	 * expression.
	 * 
	 * @param expression
	 * @return true if expression is fully parenthesized and false otherwise
	 */
	public static boolean isFPExpression(Expression expression) {
		// This method is okay. Don't have to change this.
		return isFPExpression(expression.toString());
	}

	/**
	 * Given a String check if the string represents a fully parenthesized
	 * expression.
	 * 
	 * @param expression
	 * @return true if expression is fully parenthesized and false otherwise
	 */
	public static boolean isFPExpression(String expression) {
		// TODO: Implement this method
		return true; // change this
	}

}
