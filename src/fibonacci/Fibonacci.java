package fibonacci;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class Fibonacci {

	/**
	 * Given an array of ints, this method returns a sorted List that contains
	 * the Fibonacci numbers in the input list. The list returned contains the
	 * Fibonacci numbers in ascending order and has no duplicates.
	 * 
	 * @param inputArray
	 * @return a list of all the Fibonacci numbers in inputArray, in ascending
	 *         order, with no duplicates. If the input array is null or does not
	 *         contain any Fibonacci numbers then an empty list is returned.
	 */
	public static List<Integer> getFibonacciNumbers_sorted(Integer[] inputArray) {
		// TODO: Implement this method
		List<Integer> myList = new ArrayList<Integer>();
		List<Integer> myFibonacciList = new ArrayList<Integer>();
		//if the input array is null or empty, return an empty list
		if(inputArray == null || (inputArray.length == 0)){
			return new ArrayList<Integer>();
		} else {
			myList = Arrays.asList(inputArray);
		
			HashSet<Integer> myHashSet = new HashSet<Integer>();
			
			myHashSet.addAll(myList);
			
			myList = new ArrayList<Integer>();
			//no more duplicates in myList!
			myList.addAll(myHashSet);
		
			//iterate through the list and find if a number is fibonacci
			for(int i = 0; i < myList.size(); i++){
				if(Fibonacci.CheckIfFibonacci(myList.get(i)) == true){
					myFibonacciList.add(myList.get(i));
				}
			}
			//if no fibonacci numbers return an empty list
			if(myFibonacciList.isEmpty()){
				return new ArrayList<Integer>();
			}
					Collections.sort(myFibonacciList);
					
			       
			       	
	}
		return myFibonacciList;
	}
	//helper method to see if number is fibonacci
	private static boolean CheckIfFibonacci(int n){ 
		int f1 = 0;
		int f2 = 1;
		while(f2 <= n){
			int save = f1;
			f1 = f2;
			f2 = save + f2;
			if (f2 == n){
				return true;
			}
		} 
		if(f2 > n){
          return false;
        }
		return false;
		}
	}